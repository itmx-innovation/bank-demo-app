export const environment = {
    production: true,
    apiEndpoint: 'http://ec2-54-255-189-79.ap-southeast-1.compute.amazonaws.com:3000/api/',
    bank: {
      bankId: '003',
      bankName: 'Bank C',
      tel: '02-345-3333',
      email: 'cus_support@bankc.co.th'
    },
    partnerBanks: [
      {
        bankId: '001',
        bankName: 'Bank A'
      },
      {
        bankId: '002',
        bankName: 'Bank B'
      }
    ],
    socketUrl: 'http://ec2-54-255-189-79.ap-southeast-1.compute.amazonaws.com:3000'
  };
  