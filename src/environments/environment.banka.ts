export const environment = {
    production: true,
    apiEndpoint: 'http://ec2-54-169-245-155.ap-southeast-1.compute.amazonaws.com:3000/api/',
    bank: {
      bankId: '001',
      bankName: 'Bank A',
      tel: '02-123-1111',
      email: 'customer.support@banka.co.th'
    },
    partnerBanks: [
      {
        bankId: '002',
        bankName: 'Bank B'
      },
      {
        bankId: '003',
        bankName: 'Bank C'
      }
    ],
    socketUrl: 'http://ec2-54-169-245-155.ap-southeast-1.compute.amazonaws.com:3000'
  };
  