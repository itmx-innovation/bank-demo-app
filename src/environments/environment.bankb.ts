export const environment = {
    production: true,
    apiEndpoint: 'http://ec2-54-169-113-197.ap-southeast-1.compute.amazonaws.com:3000/api/',
    bank: {
      bankId: '002',
      bankName: 'Bank B',
      tel: '02-234-2222',
      email: 'support@bankb.co.th'
    },
    partnerBanks: [
      {
        bankId: '001',
        bankName: 'Bank A'
      },
      {
        bankId: '003',
        bankName: 'Bank C'
      }
    ],
    socketUrl: 'http://ec2-54-169-113-197.ap-southeast-1.compute.amazonaws.com:3000'
  };
  