// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiEndpoint: 'http://ec2-54-169-245-155.ap-southeast-1.compute.amazonaws.com:3000/api/',
  bank: {
    bankId: '001',
    bankName: 'Bank A',
    tel: '02-123-1111',
    email: 'customer.support@banka.co.th'
  },
  partnerBanks: [
    {
      bankId: '002',
      bankName: 'Bank B'
    },
    {
      bankId: '003',
      bankName: 'Bank C'
    }
  ],
  socketUrl: 'http://ec2-54-169-245-155.ap-southeast-1.compute.amazonaws.com:3000/'
};

