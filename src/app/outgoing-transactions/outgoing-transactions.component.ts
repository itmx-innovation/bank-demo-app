import { Component, OnInit, AfterViewInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BulkDataService, BulkTransaction, BulkTransactionStatus } from '../service/bulk-data.service';

import { MatTableDataSource, MatPaginator} from '@angular/material';

@Component({
  selector: 'outgoing-transactions',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './outgoing-transactions.component.html',
  styleUrls: ['./outgoing-transactions.component.css']
})
export class OutgoingTransactionsComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns = ['ref', 'sndBranch', 'sndAccNum', 'recvBank', 'recvBranch', 'recvAccNum', 'amount', 'status'];

  dataSource: MatTableDataSource<BulkTransaction> = new MatTableDataSource([]);

  transactions: BulkTransaction[] = [];

  //UI mat-chip
  statusColor = {
    'APPROVED': 'primary',
    'PROPOSED': 'none',
    'REJECTED': 'warn'
  };

  constructor(private cd: ChangeDetectorRef, private bulkData: BulkDataService) { 

  }

  ngOnInit() {
    this.bulkData.getOutgoingTransactions()
    .subscribe((txs: BulkTransaction[]) => {
      this.transactions = txs;
      this.dataSource = new MatTableDataSource(txs);
      this.dataSource.paginator = this.paginator;
      this.cd.detectChanges();
    });

    this.bulkData.getOutgoingUpdate()
    .subscribe(tx => {
      console.log(this.dataSource.data);
      this.paginator._changePageSize(this.paginator.pageSize);
      this.cd.detectChanges();
    });

    this.bulkData.getOutgoingStatusUpdate()
    .subscribe((update:BulkTransactionStatus) => {
      this.updateTransactionStatus(update);
    });
  }

  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator;
  }

  private updateTransactionStatus(status: BulkTransactionStatus){
    this.transactions.forEach((val, i, ds) => {
      if (val.Key === status.transactionKey){
        ds[i].Record.status = status.status;
        this.dataSource.data[i].Record.status = status.status;
      }
    });

    this.cd.detectChanges()

  }


  private formattingAmount(amt: string){
    let amtInt = parseInt(amt);
    return amtInt/100;
  }

  getChipColor(status: string){
    return this.statusColor[status];
  }

}
