import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BulkTransactionService } from '../service/bulk-transaction.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-dash-board',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {

  transactions: TransactionBucket;

  constructor(private bulkService: BulkTransactionService,
              private datePipe: DatePipe,
              private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
  }

}

export interface TransactionBucket {
  incoming: any[];
  outgoing: any[];
}
