import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { OutClearingComponent } from './out-clearing/out-clearing.component';
import { DashBoardComponent } from './dash-board/dash-board.component';


export const routes: Routes = [
    { path: '', component: DashBoardComponent},
    { path: 'outclearing', component: OutClearingComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);