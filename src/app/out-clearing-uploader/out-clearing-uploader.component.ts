import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UploaderOptions, UploadInput, UploadOutput } from 'ngx-uploader';
import { BulkTransactionService, Transaction } from '../service/bulk-transaction.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'out-clearing-uploader',
  templateUrl: './out-clearing-uploader.component.html',
  styleUrls: ['./out-clearing-uploader.component.css']
})
export class OutClearingUploaderComponent implements OnInit {

  options: UploaderOptions;

  uploadInput: EventEmitter<UploadInput>;

  @Output('onTransactions')
  transaction: EventEmitter<Transaction[]> = new EventEmitter<Transaction[]>();

  constructor(private bulkService: BulkTransactionService, private datePipe: DatePipe) { 
    this.uploadInput = new EventEmitter<UploadInput>();
    this.options = { concurrency: 1 };
  }

  ngOnInit() {
  }

  onUploadOutput(output: UploadOutput){
    if (output.type === 'addedToQueue'){

      var reader = new FileReader();
      var keyPrefix = this.datePipe.transform(new Date(), "yyyyMMdd");
      reader.onload = (event) => {
        let records = reader.result.split("\n");
        let transactions: Transaction[] = [];
        let header = records.shift();
        let batch = records.shift();
        records.forEach((row) => {
          transactions.push(this.bulkService.loadFromF10Record(row, keyPrefix));
        });

        this.transaction.emit(transactions);
      }

      reader.readAsText(output.file.nativeFile);
    }
  }

}
