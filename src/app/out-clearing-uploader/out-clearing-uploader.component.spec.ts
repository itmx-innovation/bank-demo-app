import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutClearingUploaderComponent } from './out-clearing-uploader.component';

describe('OutClearingUploaderComponent', () => {
  let component: OutClearingUploaderComponent;
  let fixture: ComponentFixture<OutClearingUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutClearingUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutClearingUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
