import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutClearingSummaryComponent } from './out-clearing-summary.component';

describe('OutClearingSummaryComponent', () => {
  let component: OutClearingSummaryComponent;
  let fixture: ComponentFixture<OutClearingSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutClearingSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutClearingSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
