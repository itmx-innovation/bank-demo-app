import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'out-clearing-summary',
  templateUrl: './out-clearing-summary.component.html',
  styleUrls: ['./out-clearing-summary.component.css']
})
export class OutClearingSummaryComponent implements OnInit {

  @Input('sender-bank-code')
  senderBankCode: string;

  @Input('batch-number')
  batchNumber: string;

  @Input('transactions-count')
  numTxs: number;

  @Input('total-amount')
  totAmt: number;

  @Output('onSubmit')
  submit: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  submitTransactions(){
    this.submit.emit();
  }

}
