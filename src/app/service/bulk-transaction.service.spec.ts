import { TestBed, inject } from '@angular/core/testing';

import { BulkTransactionService } from './bulk-transaction.service';

describe('BulkTransactionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BulkTransactionService]
    });
  });

  it('should be created', inject([BulkTransactionService], (service: BulkTransactionService) => {
    expect(service).toBeTruthy();
  }));
});
