import { TestBed, inject } from '@angular/core/testing';

import { BulkDataService } from './bulk-data.service';

describe('BulkDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BulkDataService]
    });
  });

  it('should be created', inject([BulkDataService], (service: BulkDataService) => {
    expect(service).toBeTruthy();
  }));
});
