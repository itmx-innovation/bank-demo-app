import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions }  from '@angular/http';
import { environment } from '../../environments/environment';
import { DatePipe } from '@angular/common';

@Injectable()
export class BulkTransactionService {

  constructor(private http: Http, private datePipe: DatePipe) { }

  submitTransaction(transaction: Transaction){
    let options = new RequestOptions();
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    options.headers = headers;

    let api = environment.apiEndpoint + 'transactions';
    return this.http.post(api, transaction, options);
  }

  queryTransactionByDate(date: string, bankId: string){
    let options = new RequestOptions();
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    options.headers = headers;
    
    let api = environment.apiEndpoint + 'transaction/' + bankId +'?date=' + date;
    return this.http.get(api, options);
  }

  loadFromF10Record(record: string, keyPrefix: string): Transaction{
    if (record.length != 320)
      throw new Error('Incorrect format');

    // Make Key
    let key = 'C' + this.datePipe.transform(new Date(), 'yyyyMMddHHmmss') + record.slice(289,295);

    return {
      key: key,
      batchNumber: record.slice(3,9),
      receiverBankCode: record.slice(9,12),
      receiverBranchCode: record.slice(12, 16),
      receiverAccountNumber: record.slice(16, 27),
      senderBankCode: record.slice(27, 30),
      senderBranchCode: record.slice(30, 34),
      senderAccountNumber: record.slice(34, 45),
      effective: record.slice(45, 53),
      transactionType: record.slice(53, 55),
      amount: record.slice(57, 69),
      receiverInfo: record.slice(69, 129),
      senderInfo: record.slice(129, 189),
      additionalInfo: record.slice(189, 289),
      originRef: record.slice(289, 295)
    }
  }

  populateTransaction(numRecs: number): Transaction[] {
    var transactions: Transaction[] = [];
    for (var i=0; i < numRecs; i++){
      let item: Transaction = {
        key: 'C' + this.datePipe.transform(new Date(), 'yyyyMMddHHmmss') + 'REF' + this.padding(i, 3),
        batchNumber: this._randomDigit(3),
        receiverBankCode: this._randomRecBankCode(),
        receiverBranchCode: this._randomDigit(4),
        receiverAccountNumber: this._randomDigit(11),
        senderBankCode: environment.bank.bankId,
        senderBranchCode: this._randomDigit(4),
        senderAccountNumber: this._randomDigit(11),
        effective: this.datePipe.transform(new Date(), 'ddMMyyyy'),
        transactionType: 'C',
        amount: this.padding(Math.floor(Math.random() * Math.floor(3000000)), 12),
        receiverInfo: ' '.repeat(60),
        senderInfo: ' '.repeat(60),
        additionalInfo: ' '.repeat(100),
        originRef: 'REF' + this.padding(i, 3)
      };
      transactions.push(item);
    }

    return transactions;
  }

  private padding(num: number, totLen: number){
    var numStr = num.toString();
    var padding = "0".repeat(totLen - numStr.length);
    return padding + numStr;
  }

  private _randomDigit(numDigit: number){
    var rand = '';
    for (var i = 0; i < numDigit; i++){
      rand += Math.floor(Math.random() * Math.floor(10)).toString();
    }
    return rand;
  }

  private _randomRecBankCode(){
    var randIdx = Math.floor(Math.random() * Math.floor(2));
    return environment.partnerBanks[randIdx].bankId;
  }
}


export interface Transaction {
  key: string
  batchNumber: string
  senderBankCode: string
  senderBranchCode: string
  senderAccountNumber: string
  senderInfo: string
  receiverBankCode: string
  receiverBranchCode: string
  receiverAccountNumber: string
  receiverInfo: string
  transactionType: string
  effective: string
  amount: string
  additionalInfo: string
  originRef: string
}
