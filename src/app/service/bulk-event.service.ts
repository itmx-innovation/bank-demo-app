import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

import { environment } from '../../environments/environment';

import * as socketIo from 'socket.io-client';

@Injectable()
export class BulkEventService {

  private socket;

  constructor() {
    this.socket = socketIo(environment.socketUrl).connect();
  }

  public onMessage(): Observable<ChainCodeEvent>{
    return new Observable<ChainCodeEvent>(observer => {
      this.socket.on('event', (data: ChainCodeEvent) => observer.next(data));
    });
  }

  public onStatusUpdate(event: ChainCodeEvent): Observable<ChainCodeEvent>{
    return new Observable<ChainCodeEvent>(observer => {
      if(event.event_name === 'bulkStatusEvent'){
        observer.next(event);
      }
    });
  }

}

export interface ChainCodeEvent {
  event_name: string;
  payload: string;
}
