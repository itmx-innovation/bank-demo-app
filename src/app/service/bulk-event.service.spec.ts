import { TestBed, inject } from '@angular/core/testing';

import { BulkEventService } from './bulk-event.service';

describe('BulkEventService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BulkEventService]
    });
  });

  it('should be created', inject([BulkEventService], (service: BulkEventService) => {
    expect(service).toBeTruthy();
  }));
});
