import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as socketIo from 'socket.io-client';

@Injectable()
export class BulkDataService {

  private txIncoming = new Subject<BulkTransaction[]>();
  private txOutgoing = new Subject<BulkTransaction[]>();

  private updateIncoming = new Subject<BulkTransaction>();
  private updateOutgoing = new Subject<BulkTransaction>();

  // Chaincode events
  private updateIncomingStatus = new Subject<BulkTransactionStatus>();
  private updateOutgoingStatus = new Subject<BulkTransactionStatus>();

  private socket;

  private incoming: BulkTransaction[] = [];
  private outgoing: BulkTransaction[] = [];

  private initialized: boolean = false;

  constructor(private http: Http, private datePipe: DatePipe) { 
    this.socket = socketIo();
    this.socket.connect(environment.socketUrl, {'path:': '/'});
    this.onChaincodeEvent();
  }

  // Full payload from incoming transaction
  getIncomingTransactions(): Observable<BulkTransaction[]> {
    if (!this.initialized){
      this.loadData();
      this.initialized = true;
    }
    
    return this.txIncoming.asObservable();
  }

  // Full payload from out going transaction
  getOutgoingTransactions(): Observable<BulkTransaction[]> {

    if (!this.initialized){
      this.loadData();
      this.initialized = true;
    }

    return this.txOutgoing.asObservable();
  }

  getOutgoingStatusUpdate(): Observable<BulkTransactionStatus>{
    return this.updateOutgoingStatus.asObservable();
  }

  getOutgoingUpdate(): Observable<BulkTransaction>{
    return this.updateOutgoing.asObservable();
  }

  getIncomingStatusUpdate(): Observable<BulkTransactionStatus>{
    return this.updateIncomingStatus.asObservable();
  }

  getIncomingUpdate(): Observable<BulkTransaction> {
    return this.updateIncoming.asObservable();
  }

  approveTransaction(tx: BulkTransaction){
    return this.updateTransactionStatus(tx, true);
  }

  rejectTransaction(tx: BulkTransaction){
    return this.updateTransactionStatus(tx, false);
  }

  private updateTransactionStatus(tx: BulkTransaction, status: boolean){
    let api = environment.apiEndpoint + 'transactions/' + tx.Record.sendingBankCode + '/' + tx.Key;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let opt = new RequestOptions();
    opt.headers = headers;

    let body:UpdateStatus = { status: (status)? "true": "false"};

    return this.http.put(api, body, opt);
  }

  // Load data 
  // For demo purpose, only load today data.
  private loadData(){

    let today = this.datePipe.transform(new Date(), 'yyyyMMdd');
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let opt = new RequestOptions();
    opt.headers = headers;

    this.incoming = [];
    this.outgoing = [];

    environment.partnerBanks.forEach((bank) => {
      let api = environment.apiEndpoint + 'transactions/' + bank.bankId + '?date=' + today;
      this.http.get(api, opt)
      .subscribe((resp) => {
        resp.json().forEach((tx) => {
          this.multiplexingTransaction(tx);
        });
        this.txIncoming.next(this.incoming);
        this.txOutgoing.next(this.outgoing);
      });
    });
  }


  // Multiplexing transaction either incoming or outgoing
  private multiplexingTransaction(tx){
    let transaction: BulkTransaction = tx;
    transaction.Record.transferAmount = parseInt(tx.Record.transferAmount)/100;

    let record = transaction.Record;

    record.status = this.adjustStatusTxt(record.status);

    if (record.sendingBankCode === environment.bank.bankId){
      this.outgoing.unshift(transaction);
    } else {
      this.incoming.unshift(transaction);
    }
  }

  private onChaincodeEvent(){
    this.socket.on('event', (ev: ChainCodeEvent) => {
      // Status update event
      if (ev.event_name === 'bulkStatusEvent'){
        let payload: BulkTransactionStatus = JSON.parse(ev.payload);

        payload.status = this.adjustStatusTxt(payload.status);
        // Incoming or Outgoing transaction
        if (payload.senderBank === environment.bank.bankId){
          this.updateOutgoingStatus.next(payload);
        } else {
          this.updateIncomingStatus.next(payload);
        }
      } else if (ev.event_name === 'bulkTransactionProposedEvent'){
        let event: BulkTransactionProposed = JSON.parse(ev.payload);

        // Make http headers for request api
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let opt = new RequestOptions();
        opt.headers = headers;

        if (event.senderBank === environment.bank.bankId){ // Outgoing transaction
          let api = environment.apiEndpoint + 'transactions/' + event.receiverBank + '/' + event.transactionKey;
          this.http.get(api, opt)
          .subscribe((resp:Response) => {
            if (resp.status === 200){
              let body = resp.json();
              let data: BulkTransaction = { Key: event.transactionKey, Record: body};
              data.Record.transferAmount = parseInt(body.transferAmount)/100;
              this.updateOutgoing.next(data);
            }
          })
        } else if (event.receiverBank === environment.bank.bankId){ // Incoming transaction
          let api = environment.apiEndpoint + 'transactions/' + event.senderBank + '/' + event.transactionKey;
          this.http.get(api, opt)
          .subscribe((resp: Response) => {
            if (resp.status === 200){
              let body = resp.json();
              let data: BulkTransaction = {Key: event.transactionKey, Record: body};
              data.Record.transferAmount = parseInt(body.transferAmount)/100;
              this.updateIncoming.next(data);
            }
          });
        }

      }
    });
  }

  private adjustStatusTxt(status: string){
    if (status === 'PROCESS_SUCCEED'){
      return 'APPROVED';
    } else if (status === 'PROCESS_FAILED'){
      return 'REJECTED';
    } else { return status; }
  }


}

export interface BulkTransaction {
  Key: string;
  Record: {
    BatchNumber: string;
    effectiveTransferDate: string;
    orgRefNumber: string;
    otherInfo: string;
    receiverInfo: string;
    receivingBankCode: string;
    receivingBranchCode: string;
    receivingBankAccNum: string;
    senderInfo: string;
    sendingBankCode: string;
    sendingBranchCode: string;
    sendingBankAccNum: string;
    serviceTypeCode: string;
    transferAmount: number;
    status: string;
  }
}

export interface BulkTransactionStatus {
  senderBank: string;
  receiverBank: string;
  transactionKey: string;
  status: string;
}

export interface BulkTransactionProposed {
  senderBank: string;
  receiverBank: string;
  transactionKey: string;
}

export interface ChainCodeEvent{
  event_name: string;
  payload: string;
}

export interface UpdateStatus {
  status: string;
}
