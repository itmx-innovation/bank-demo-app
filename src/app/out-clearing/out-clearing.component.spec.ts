import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutClearingComponent } from './out-clearing.component';

describe('OutClearingComponent', () => {
  let component: OutClearingComponent;
  let fixture: ComponentFixture<OutClearingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutClearingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutClearingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
