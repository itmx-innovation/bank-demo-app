import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Transaction, BulkTransactionService } from '../service/bulk-transaction.service';

@Component({
  selector: 'out-clearing',
  templateUrl: './out-clearing.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./out-clearing.component.css']
})
export class OutClearingComponent implements OnInit {

  transactions: TransactionWrapper[] = [];

  totAmount = 0;

  done = 0;

  constructor(private cd: ChangeDetectorRef, private bulkService: BulkTransactionService) { }

  ngOnInit() {
  }

  isShowUpload(){
    if(this.transactions.length > 0) return false;
    else return true;
  }

  onTransactions(transactions){
    transactions.forEach((tx: Transaction) => {
      this.totAmount += parseInt(tx.amount);
      this.transactions.push({transaction: tx, status: 'NEW'});
    });
  }

  populateTransaction(){
    this.onTransactions(this.bulkService.populateTransaction(10));
  }

  getAmount(){
    return this.totAmount/100;
  }

  getBatchNumber(){
    if (this.transactions.length > 0){
      return this.transactions[0].transaction.batchNumber;
    } else return "";
  }

  getNumberTxs(){
    return this.transactions.length;
  }

  getSenderBankCode(){
    if (this.transactions.length > 0){
      return this.transactions[0].transaction.senderBankCode;
    } else return "";
  }

  getCompletedPercent(){
    return (this.done * 100)/this.transactions.length;
  }

  onSubmit(){
    this.done = 0;
    this.transactions.forEach((tx, i) => {
      setTimeout(() => {

        this.bulkService.submitTransaction(tx.transaction)
        .subscribe((res) => {
          this.done += 1;
          console.log(this.done);
          this.transactions[i].status = 'PROPOSED';
          this.cd.markForCheck();
        });
      }, i*200);
      
    });
  }

}

export interface TransactionWrapper {
  transaction:Transaction,
  status: string
}
