import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BulkDataService, BulkTransaction, BulkTransactionStatus } from '../service/bulk-data.service';

@Component({
  selector: 'transaction-status-chart',
  changeDetection: ChangeDetectionStrategy.Default,
  templateUrl: './transaction-status-chart.component.html',
  styleUrls: ['./transaction-status-chart.component.css']
})
export class TransactionStatusChartComponent implements OnInit {

  single: any[];
  multi: any[];

  view: any[] = [800, 200];

  // options
  showLegend = true;

  colorScheme = {
    domain: ['#9E9E9E', '#2f3e9e', '#D32F2F', '#AAAAAA']
  };

  // pie
  showLabels = true;
  explodeSlices = false;
  doughnut = true;

  transactions: BulkTransaction[];

  sampleData = [
    {
      "name": "Pending",
      "value": 0
    },
    {
      "name": "Approved",
      "value": 0
    },
    {
      "name": "Rejected",
      "value": 0
    }
  ]

  constructor(private bulkData: BulkDataService, private cd: ChangeDetectorRef) { 
    
  }

  ngOnInit() {

    this.bulkData.getOutgoingTransactions()
    .subscribe((txs:BulkTransaction[]) => {
      this.transactions = txs;
      this.sampleData = this.calculate();
      this.cd.detectChanges();
    });

    this.bulkData.getOutgoingUpdate()
    .subscribe((tx: BulkTransaction) => {
      this.transactions.unshift(tx);
      this.sampleData = this.calculate();
      this.cd.detectChanges();
    });

    this.bulkData.getOutgoingStatusUpdate()
    .subscribe((status: BulkTransactionStatus) => {
      this.transactions.forEach((tx: BulkTransaction, i, txs) => {
        if(tx.Key === status.transactionKey){
          txs[i].Record.status = status.status;
        }
      });

      this.sampleData = this.calculate();
      this.cd.detectChanges();
    });
  }

  private calculate(){
    return [
      {
        "name": "Pending",
        "value": this.transactions.filter((tx: BulkTransaction) => tx.Record.status==='PROPOSED').length
      },
      {
        "name": "Approved",
        "value": this.transactions.filter((tx: BulkTransaction)=> tx.Record.status==='APPROVED').length
      },
      {
        "name": "Reject",
        "value": this.transactions.filter((tx: BulkTransaction)=> tx.Record.status==='REJECTED').length
      }
    ];
    /** 
    this.sampleData[0].value = this.transactions.filter((tx:BulkTransaction) => {
      return (tx.Record.status === 'PROPOSED')? true : false;
    })
    .length;

    this.sampleData[1].value = this.transactions.filter((tx: BulkTransaction) => {
      console.log(tx.Record.status);
      return (tx.Record.status === 'APPROVED')? true: false;
    })
    .length;

    this.sampleData[2].value = this.transactions.filter((tx: BulkTransaction) => {
      return (tx.Record.status === 'REJECTED')? true: false;
    })
    .length;

    console.log(this.sampleData);
    */
  }

}
