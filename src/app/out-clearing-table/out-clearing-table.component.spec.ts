import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutClearingTableComponent } from './out-clearing-table.component';

describe('OutClearingTableComponent', () => {
  let component: OutClearingTableComponent;
  let fixture: ComponentFixture<OutClearingTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutClearingTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutClearingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
