import { Component, OnInit, Input } from '@angular/core';
import { Transaction } from '../service/bulk-transaction.service';

@Component({
  selector: 'out-clearing-table',
  templateUrl: './out-clearing-table.component.html',
  styleUrls: ['./out-clearing-table.component.css']
})
export class OutClearingTableComponent implements OnInit {

  @Input('transactions')
  transactions: Transaction[];

  constructor() { }

  ngOnInit() {
  }

  formattingAmount(amt: string){
    let amtInt = parseInt(amt);
    return amtInt/100;
  }

}
