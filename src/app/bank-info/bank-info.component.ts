import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';

@Component({
  selector: 'bank-info',
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.css']
})
export class BankInfoComponent implements OnInit {

  bankId = environment.bank.bankId;
  bankName = environment.bank.bankName;
  email = environment.bank.email;
  tel = environment.bank.tel;

  constructor() { }

  ngOnInit() {
  }

}
