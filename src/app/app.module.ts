import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { DatePipe } from '@angular/common';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { OutClearingUploaderComponent } from './out-clearing-uploader/out-clearing-uploader.component';
import { DashBoardComponent } from './dash-board/dash-board.component';

import { routing } from './app.routes';

// 3rd party modules/libraries
import { NgUploaderModule } from 'ngx-uploader';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { NgxChartsModule } from '@swimlane/ngx-charts';


// Material Design
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';


// Services
import { BulkTransactionService } from './service/bulk-transaction.service';
import { OutClearingComponent } from './out-clearing/out-clearing.component';
import { OutClearingTableComponent } from './out-clearing-table/out-clearing-table.component';
import { OutClearingSummaryComponent } from './out-clearing-summary/out-clearing-summary.component';
import { TodayTransactionsComponent } from './today-transactions/today-transactions.component';
import { BulkEventService } from './service/bulk-event.service';
import { OutgoingTransactionsComponent } from './outgoing-transactions/outgoing-transactions.component';
import { IncomingTransactionsComponent } from './incoming-transactions/incoming-transactions.component';
import { NetPositionsComponent } from './net-positions/net-positions.component';
import { BulkDataService } from './service/bulk-data.service';
import { BankInfoComponent } from './bank-info/bank-info.component';
import { TransactionStatusChartComponent } from './transaction-status-chart/transaction-status-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    OutClearingUploaderComponent,
    DashBoardComponent,
    OutClearingComponent,
    OutClearingTableComponent,
    OutClearingSummaryComponent,
    TodayTransactionsComponent,
    OutgoingTransactionsComponent,
    IncomingTransactionsComponent,
    NetPositionsComponent,
    BankInfoComponent,
    TransactionStatusChartComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    NgUploaderModule,
    HttpModule,
    NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#780C000",
      innerStrokeColor: "#C7E596"
    }),
    NgxChartsModule,
    MatTabsModule,
    MatCardModule,
    MatChipsModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
  ],
  providers: [
    BulkTransactionService,
    BulkEventService,
    BulkDataService,
    DatePipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
