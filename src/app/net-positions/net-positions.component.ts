import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

import { environment } from '../../environments/environment';
import { BulkDataService, BulkTransaction, BulkTransactionStatus } from '../service/bulk-data.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'net-positions',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './net-positions.component.html',
  styleUrls: ['./net-positions.component.css']
})
export class NetPositionsComponent implements OnInit {

  positions: NetPosition[] = [];

  incoming: BulkTransaction[] = [];
  outgoing: BulkTransaction[] = [];


  // For material Table
  dataSource: MatTableDataSource<NetPosition>;
  displayedColumns = ['bankName', 'bankCode', 'net'];

  constructor(private cd: ChangeDetectorRef, private bulkData: BulkDataService) {
    environment.partnerBanks.forEach((bank) => {
      this.positions.push({bankId: bank.bankId, bankName: bank.bankName, position: 0});
    });

    this.dataSource = new MatTableDataSource(this.positions);
  }

  ngOnInit() {
    this.bulkData.getIncomingTransactions()
    .subscribe((txs: BulkTransaction[]) => {
      this.incoming = [];
      txs.forEach((tx) => {
        this.incoming.unshift(tx);
      });

      this.caculate();
    });

    this.bulkData.getOutgoingTransactions()
    .subscribe((txs: BulkTransaction[]) => {
      this.outgoing = [];
      txs.forEach((tx) => {
        this.outgoing.unshift(tx);
      });
      this.caculate();
    });

    this.bulkData.getIncomingUpdate()
    .subscribe((tx: BulkTransaction) => {
      this.incoming.unshift(tx);
      this.caculate();
    });

    this.bulkData.getOutgoingUpdate()
    .subscribe((tx: BulkTransaction) => {
      this.outgoing.unshift(tx);
      this.caculate();
    });

    this.bulkData.getIncomingStatusUpdate()
    .subscribe((status: BulkTransactionStatus) => {
      this.incoming.forEach((tx, i, ds) => {
        if(tx.Key === status.transactionKey ){
          ds[i].Record.status = status.status;
        }
      });
      this.caculate();
    });

    this.bulkData.getOutgoingStatusUpdate()
    .subscribe((status: BulkTransactionStatus) => {
      this.outgoing.forEach((tx, i, ds) => {
        if (tx.Key === status.transactionKey){
          ds[i].Record.status = status.status;
        }
      });
      this.caculate();
    });
  }

  getChipColor(position: number){
    return (position < 0)? "warn" : "primary";
  }

  private caculate(){
    environment.partnerBanks.forEach((bank, i) => {
      let onet = this.outgoing.filter((val: BulkTransaction) => val.Record.receivingBankCode === bank.bankId)
                .map((val) => val.Record.transferAmount)
                .reduce((a:number, b:number) => { return a+b}, 0);

      let inet = this.incoming.filter((val: BulkTransaction) => val.Record.sendingBankCode === bank.bankId)
                .map((val) => val.Record.transferAmount)
                .reduce((a:number, b:number) => {return a+b}, 0);
      this.positions[i] = { bankId: bank.bankId, bankName: bank.bankName, position: inet-onet}
      this.dataSource = new MatTableDataSource(this.positions);
      this.cd.detectChanges();
    });

  }

}

export interface NetPosition {
  bankId: string;
  bankName: string;
  position: number;
}
