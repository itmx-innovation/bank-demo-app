import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { BulkDataService, BulkTransaction, BulkTransactionStatus } from '../service/bulk-data.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'incoming-transactions',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './incoming-transactions.component.html',
  styleUrls: ['./incoming-transactions.component.css']
})
export class IncomingTransactionsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns = ['select', 'ref', 'recvBranch', 'recvAccNum', 'sndBank', 'sndBranch', 'sndAccNum', 'amount', 'status'];

  dataSource: MatTableDataSource<BulkTransaction> = new MatTableDataSource([]);

  transactions: BulkTransaction[] = [];

  initialSelection = [];
  allowMultiSelect = true;
  selection:SelectionModel<BulkTransaction>;

  //UI mat-chip
  statusColor = {
    'APPROVED': 'primary',
    'PROPOSED': 'none',
    'REJECTED': 'warn'
  };

  constructor(private cd: ChangeDetectorRef, private bulkData: BulkDataService) { 
    this.selection = new SelectionModel(this.allowMultiSelect, this.initialSelection);
  }

  ngOnInit() {

    // Load data
    this.bulkData.getIncomingTransactions()
    .subscribe((txs: BulkTransaction[]) => {
      this.transactions = txs;
      this.dataSource = new MatTableDataSource(this.transactions);
      this.dataSource.paginator = this.paginator;
      this.cd.detectChanges();
    });

      
    // Handle new incoming record status
    this.bulkData.getIncomingStatusUpdate()
    .subscribe((status: BulkTransactionStatus) => {
      this.updateTransactionStatus(status);
    });

    // Handle new incoming record
    this.bulkData.getIncomingUpdate()
    .subscribe((tx: BulkTransaction) =>{
      let duplicated = this.transactions.filter((t:BulkTransaction) => {
                          return (tx.Key === t.Key)? true: false;
                        }).length

      if (duplicated === 0){
        this.transactions.unshift(tx);
        this.dataSource = new MatTableDataSource(this.transactions);
        this.dataSource.paginator = this.paginator;
      } else {
        this.paginator._changePageSize(this.paginator.pageSize);
      }

      this.cd.detectChanges();
    }); 
  }

  private updateTransactionStatus(status: BulkTransactionStatus){
    this.transactions.forEach((val, i, ds) => {
      if(val.Key === status.transactionKey){
        ds[i].Record.status = status.status;
        this.dataSource.data[i].Record.status = status.status;
      }
    });

    this.cd.detectChanges();
  }

  approveTransaction(){
    this.selection.selected.forEach((tx: BulkTransaction) => {
      this.bulkData.approveTransaction(tx)
      .subscribe(resp => {
        this.selection.deselect(tx);
      });
    });
  }

  rejectTransaction(){
    this.selection.selected.forEach((tx: BulkTransaction) => {
      this.bulkData.rejectTransaction(tx)
      .subscribe(resp => {
        this.selection.deselect(tx);
      });
    });
  }

  getChipColor(status: string){
    return this.statusColor[status];
  }

    /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  isSelected(){
    return (this.selection.selected.length === 0)? false : true;
  }

}
