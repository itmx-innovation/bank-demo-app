import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodayTransactionsComponent } from './today-transactions.component';

describe('TodayTransactionsComponent', () => {
  let component: TodayTransactionsComponent;
  let fixture: ComponentFixture<TodayTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodayTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodayTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
