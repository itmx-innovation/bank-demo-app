import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BulkTransactionService, Transaction } from '../service/bulk-transaction.service';
import { DatePipe } from '@angular/common';
import { BulkEventService } from '../service/bulk-event.service';

@Component({
  selector: 'today-transactions',
  templateUrl: './today-transactions.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./today-transactions.component.css']
})
export class TodayTransactionsComponent implements OnInit {

  transactions: any[] = [];

  constructor(private bulkService: BulkTransactionService, 
              private datePipe: DatePipe,
              private cd: ChangeDetectorRef,
              private eventService: BulkEventService) {
    let date = datePipe.transform(new Date(), "yyyyMMdd");
    this.bulkService.queryTransactionByDate(date, '002')
    .subscribe((results) => {
      results.json().forEach((tx) => {
        this.transactions.push(tx);
        this.cd.detectChanges();
      });
    });

    this.bulkService.queryTransactionByDate(date, '003')
    .subscribe((results) => {
      results.json().forEach((tx) => {
        this.transactions.push(tx);
        this.cd.detectChanges();
      });
    });

    this.eventService.onMessage()
    .subscribe(event => {
      if(event.event_name === 'bulkStatusEvent'){
        let payload = JSON.parse(event.payload);
        console.log(payload.transactionKey);
        this.transactions.forEach((tx, i) => {
          if (tx.Key == payload.transactionKey){
            console.log("Update ui now!!!!");
            this.transactions[i].Record.status = payload.status;
            this.cd.markForCheck();
          }
        });
      }
    });
  }

  ngOnInit() {
  }

  formattingAmount(amt: string){
    let amtInt = parseInt(amt);
    return amtInt/100;
  }

}
