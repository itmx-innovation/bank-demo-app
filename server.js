const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');

const api = require('./api/api');
const evh = require('./api/handler/chaincodeEvent');

const app = new express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'dist')));

app.use(function(req, res, next){
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Content-Length');

	if ('OPTIONS' === req.methods){
		res.header('Access-Control-Allow-Methods', 'POST,PUT,GET,OPTIONS');
		res.sendStatus(200);
	} else {
		next();
	}
});

app.use('/api', api);

app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// Register for chaincode events
evh.EventHub();
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);


const io = require('socket.io').listen(server);
console.log('setup socket.io');
io.sockets.on('connect', function(socket){
	console.log('Client App is connected');
	evh.eventEmitter.on('bulkEvent', function(data){
		// Send data out to client app
		console.log('emit event to client via socket.io');
		socket.emit('event', data);
	});
});

server.listen(port, ()=> console.log(`Bulk payment app is starting on port ${port}`));
