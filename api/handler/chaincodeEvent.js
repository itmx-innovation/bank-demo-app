
const EventHub = require('../service/eventHubService');
const config = require('../../config/channels');
const path = require('path');
const EventEmitter = require('events').EventEmitter;

const eventEmitter = new EventEmitter();


const opts = {
	username: process.env.BANKDEMO_USERNAME,
	mspid: process.env.BANKDEMO_MSPID,
	cryptoContent: {
		privateKey: process.env.BANKDEMO_USER_PRIVATE_KEY,
		signedCert: process.env.BANKDEMO_USER_SIGNED_CERT
	},
	skipPersistence: false
};


function onEvent(e){
	// Emit event
	let data = { event_name: e.event_name, payload: e.payload.toString('utf8') };
	eventEmitter.emit('bulkEvent', data);
}

function onError(e){
	console.log('Got error event');
	console.log(e);
}

exports.EventHub = function (){
    ev = new EventHub(process.env.BANKDEMO_EVENTHUB_ADDRESS, opts);
    ev.subscribeEvent(onEvent, onError);
}

exports.eventEmitter = eventEmitter;
