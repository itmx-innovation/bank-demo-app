const path = require('path');
const Bulk = require('../service/bulkPaymentService');
const config = require('../../config/channels');

const opts = {
	username: process.env.BANKDEMO_USERNAME,
	mspid: process.env.BANKDEMO_MSPID,
	cryptoContent: {
		privateKey: process.env.BANKDEMO_USER_PRIVATE_KEY,
		signedCert: process.env.BANKDEMO_USER_SIGNED_CERT
	},
	skipPersistence: true
};

var channels = {
};

// Setup channels
var bankId = 'b' + process.env.BANKDEMO_BANKID;
for (var key in config){
	if (key !== bankId){
		console.log(`Create channel for ${key}`);
		channels[key] = new Bulk(config[bankId][key], process.env.BANKDEMO_PEER_ADDRESS, process.env.BANKDEMO_ORDERER_ADDRESS);
	}
}

exports.submitF10TransactionHandler = function(req, res){
	let tx = req.body;

	let args = [
		tx.key,
		tx.batchNumber,
		tx.receiverBankCode,
		tx.receiverBranchCode,
		tx.receiverAccountNumber,
		tx.receiverInfo,

		tx.senderBankCode,
		tx.senderBranchCode,
		tx.senderAccountNumber,
		tx.senderInfo,

		tx.effective,
		tx.transactionType,
		tx.amount,
		tx.additionalInfo,

		tx.originRef
	];

	//console.log(args);
	let channelId = 'b'+tx.receiverBankCode;
	let bulk = channels[channelId];
	console.log(`Send transaction to channel: ${channelId}`);

	bulk.loadUser(opts)
	.then((user) => {
		// Submit transaction
		return bulk.submitTransaction(args);
		
	})
	.then((tx_id) => {
		console.log(`${tx_id} was successfully committed`);
		res.statusCode = 201;
		return res.json({txId: tx_id});	
	})
	.catch((err)=> {
		console.error(err);
		res.statusCode = 401;
		return res.json({ errors: [`Unauthorized`] });
	});
}

exports.getTransactionHandler = function(req, res){
	let bulk = channels['b'+req.params.bankId];
	bulk.loadUser(opts)
	.then((user) => {
		return bulk.getTransaction(req.params.key);
	})
	.then((payload) => {
		res.statusCode = 200;
		return res.json(JSON.parse(payload.toString('utf8')));
	})
	.catch((err) => {
		res.statusCode = 500;
		return res.json({error: err});
	})
}

exports.updateTransactionHandler = function(req, res){
	let bulk = channels['b'+req.params.senderBankId];
	let status = req.body.status;
	let key = req.params.key;

	console.log(`Update transaction key: ${key}, with status:${status} on channel b${req.params.senderBankId}`);

	bulk.loadUser(opts)
	.then((user) => {
		console.log('load user success, procceed to update transaction');
		return bulk.updateTransaction(key, status);
	})
	.then((tx_id) => {
		res.statusCode = 200;
		return res.json({tx_id: tx_id, status: 'SUCCESS'});
	})
	.catch((err) => {
		console.log('some error occure');
		res.statusCode = 500;
		return res.json({error: err});
	});
}

exports.getTransactionsByRange = function(req, res){
	let bulk = channels['b'+ req.params.bankId];
	let start = 'C' + req.query.date + '000000';
	let end = 'C' + req.query.date + '235959999999';
	bulk.loadUser(opts)
	.then((user) => {
		return bulk.queryByRange(start, end)
	})
	.then((results) => {
		res.statusCode = 200;
		return res.json(JSON.parse(results));
	})
	.catch((err) => {
		res.statusCode = 500;
		return res.json({error: err});
	});
}
