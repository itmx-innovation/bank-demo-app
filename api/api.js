const express = require('express');
const transactionHandler = require('./handler/transaction');

const router = express.Router();

router.post('/transactions', transactionHandler.submitF10TransactionHandler);
router.get('/transactions/:bankId/:key', transactionHandler.getTransactionHandler);
router.get('/transactions/:bankId', transactionHandler.getTransactionsByRange);
router.put('/transactions/:senderBankId/:key', transactionHandler.updateTransactionHandler);

module.exports = router;
