const path = require('path');
const fs = require('fs');

const recBankCodes = ["002", "003"];
const recBranchCodes = ["1234", "2345", "0010", "0234", "0293", "1203", "1293"]

function generateHeaderRecord(){
    var fileType = "10";
    var recordType = "0";
    var versionRelease = "0".repeat(8);
    var releasedDate = "0".repeat(8)
    var padding = " ".repeat(301);
    return fileType+recordType+versionRelease+releasedDate+padding;
}

function generateBatchRecord(batchNumber, senderBankCode, txsInBatch, totAmt, effectiveDate, txType, productCode, companyCode){
    var fileType = "10";
    var recordType = "1";
    
    return fileType + recordType + _padding(batchNumber, 6) + senderBankCode + _padding(txsInBatch, 3) + _padding(totAmt, 15)
        + effectiveDate + txType + productCode + companyCode + " ".repeat(263);
}

function generateDetailRecord(batchNumber, recBankCode, recBranchCode, recAcc, sndBankCode, 
    sndBranchCode, sndAcc, effective, serviceType, amt, ref) {
        var fileType = "10";
        var recordType = "2";

        return fileType + recordType + _padding(batchNumber, 6) + recBankCode + recBranchCode + recAcc + sndBankCode + sndBranchCode + sndAcc
            + effective + serviceType + "00" + _padding(amt, 12) + ' '.repeat(60) + ' '.repeat(60) + ' '.repeat(100) + ref + ' '.repeat(25)
}

function generateF10(filename, batchNumber, numTxs, sndBankCode, effective){
    var header = generateHeaderRecord();
    var details = [];
    var tot = 0;

    for (var i=0; i < numTxs; i++){
        var amt = Math.floor(Math.random() * Math.floor(3000000));
        var ref = 'REF' + _padding(i, 3);
        var rec = generateDetailRecord(batchNumber, _randBankCode(), _randBranchCode(), _randBankAccount(), sndBankCode,
                    _randBranchCode(), _randBankAccount(), effective, 'AA', amt, ref);
        
        details.push(rec);
        tot += amt;
    }

    var randCompanyCode = Math.floor(Math.random() * Math.floor(10000));
    var batch = generateBatchRecord(batchNumber, sndBankCode, numTxs, tot, effective, 'C', 'DC2', _padding(randCompanyCode, 15));

    var fname = path.join(__dirname, 'f10', filename);
    var stream = fs.createWriteStream(path.join(__dirname, 'f10', filename));
    stream.once('open', function(fd){
        // Write header
        stream.write(header);
        stream.write("\n");

        // Write batch record
        stream.write(batch);
        stream.write("\n");

        for(var i=0; i < details.length; i++){
            stream.write(details[i]);
            if (i != details.length-1)
                stream.write("\n");
        }
    });
    
}

function _padding(num, len){
    var numStr = num.toString();
    var padding = "0".repeat(len-numStr.length);
    return padding+numStr;
}

function _randBankCode(){
    return recBankCodes[Math.floor(Math.random()*recBankCodes.length)];
}

function _randBranchCode(){
    return recBranchCodes[Math.floor(Math.random()*recBranchCodes.length)];
}

function _randBankAccount(){
    var acc = "";
    for (var i=0; i<11; i++){
        acc += Math.floor(Math.random() * Math.floor(10)).toString();
    }
    return acc;
}

function _pringUsage(){
    console.log('[Usage]')
    console.log('node generateF10.js \n\t--out <output filename> \n\t--batch <batch number> \n\t--count <number of tx> \n\t--sender <sender bankId> \n\t--effective <effective date ddMMyyyy>');
}


var argv = require('minimist')(process.argv.slice(2));
var keys = ['out', 'batch', 'count', 'sender', 'effective'];
var isGood = true;

console.log(argv);

keys.forEach(function(k){
    if (!argv.hasOwnProperty(k)){
        isGood = false;
    }
});


if (isGood) {
    generateF10(argv.out, argv.batch, argv.count, _padding(argv.sender, 3), argv.effective);
} else {
    console.log('Insufficient arguments');
    _pringUsage();
}
