const path = require('path');
const bulkPaymentService = require('./service/bulkPaymentService');

const opts = {
	username: 'Admin',
	mspid: 'ITMXOrgMSP',
	cryptoContent: {
		privateKey: path.join(__dirname, 'crypto-material/Admin@itmx.co.th/msp/keystore/private_key'),
		signedCert: path.join(__dirname, 'crypto-material/Admin@itmx.co.th/msp/signcerts/Admin@itmx.co.th-cert.pem')
	},
	skipPersistence: false
};

const bulk = new bulkPaymentService('testchainid',
    'grpc://ec2-52-77-211-173.ap-southeast-1.compute.amazonaws.com:7051', 
    'grpc://ec2-13-250-51-166.ap-southeast-1.compute.amazonaws.com:7050');

function QueryByDate(productCode, date){
    bulk.loadUser(opts)
    .then((user) => {
        bulk.queryByDate(productCode, date)
        .then((results) => {
            console.log(results);
            console.log(results.length);
            console.log(results[0].toString('utf8'));
        })
    })
    .catch((err) => console.error(err));
}

function QueryTransaction(key){
	bulk.loadUser(opts)
	.then((user) => {
		return bulk.getTransaction(key);
	})
	.then((results) => {
		console.log(results.length);
		console.log(results[0].toString('utf8'));
		console.log(results[0].length);
	})
	.catch((err) => console.error(err));
}

function Test(key, rec){
    bulk.loadUser(opts)
        .then((user) => {
            // Submit some transaction
            let args = [
				key,
                "1000",
                "001",
                "0001",
                "12345678901",
                "Some other basic info",
                rec,
                "0023",
                "09876543123",
                "Another receiver basic info",
                "06032018",
                "AA",
                "250000",
                "Free text",
                "BBL00012345"
            ];

            return bulk.submitTransaction(args);
        })
        .then((tx_id) => {
            console.log(`${tx_id} was successfully committed`);
        })
        .catch(err => console.error(err));
}
Test('DC320180305001', '002');
Test('DC320180305002', '003');
Test('DC320180305003', '003');
Test('DC320180305004', '002');
Test('DC320180305005', '002');
//QueryByDate('DC3', '20180305');
//QueryTransaction('DC301');
