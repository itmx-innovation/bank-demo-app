const Client = require('fabric-client')
const path = require('path');
const store_path = path.join(__dirname, '../hfc-key-store');

const createTransactionFcn = 'createTransferTransaction';
const queryByDateRangeFcn = 'queryByDate';
const getTransactionFcn = 'getTransaction';
const updateRecvProcessStatusFcn = 'updateReceivingProcessStatus';
const chaincodeId = 'bulkpayment';

function BulkPaymentService(channelid, peerUri, ordererUri){

    // User who transact the payment
    this.user = null;

    this.client = new Client();
    this.channel = this.client.newChannel(channelid);

    console.log(`[Peer]: ${peerUri}`);
    let peer = this.client.newPeer(peerUri);
    this.channel.addPeer(peer);

    console.log(`[Orderer]: ${ordererUri}`);
    let orderer = this.client.newOrderer(ordererUri);
    this.channel.addOrderer(orderer);

    // Create key-value store
    Client.newDefaultKeyValueStore({path: store_path})
    .then((state_store) => {
        this.client.setStateStore(state_store);
        let crypto_suite = Client.newCryptoSuite();
        let crypto_store = Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        this.client.setCryptoSuite(crypto_suite);
    })
    .catch((err) => console.error(err));
}

/**
 * @returns: Primise<User>
 * @param {*} opt 
 * {
 *  username: string,
 *  mspid: string,
 *  cryptoContent: {
 *      privateKey: string (path to user private key),
 *      signedCert: string (path to user signed certificate)
 *  },
 *  skipPersistence: boolean
 * }
 */
BulkPaymentService.prototype.loadUser = function(opt){
    return this.client.createUser(opt)
        .then((user) => {
            this.user = user;
            return user;
        });
}


BulkPaymentService.prototype.submitTransaction = function(args){
    let tx_id = this.client.newTransactionID();

    let request = {
        chaincodeId: chaincodeId,
        fcn: createTransactionFcn,
        args: args,
        txId: tx_id
    };

    let txIdStr = tx_id.getTransactionID(); // for logging purposes

    return new Promise((resolve, reject) => {
        this.channel.sendTransactionProposal(request)
        .then((results) => {
            let proposalResponses = results[0];
            let proposal = results[1];
            let isGood = false;

            if (proposalResponses && proposalResponses[0].response &&
                proposalResponses[0].response.status === 200){
                    isGood = true;
            } else {
                reject('Proposals were rejected');
            }

            if (isGood){
                let request = {
                    proposalResponses: proposalResponses,
                    proposal: proposal
                };
                console.log(`${txIdStr}: Committing transaction`);
                return this.channel.sendTransaction(request)
            }
        })
        .then((results)=> {
            if(results.status === 'SUCCESS'){
                resolve(txIdStr);
            } else {
                reject(`${txIdStr}: commit error`);
            }
        });
    });
}


BulkPaymentService.prototype.updateTransaction = function(key, status){
	let tx_id = this.client.newTransactionID();
	let request = {
		chaincodeId: chaincodeId,
		fcn:  updateRecvProcessStatusFcn,
		args: [key, status],
		txId: tx_id
	};

	let txIdStr = tx_id.getTransactionID();


	return new Promise((resolve, reject) => {
		console.log(`Propose to blockchain with tx_id: ${txIdStr}`);
		this.channel.sendTransactionProposal(request)
		.then((results) => {
			let proposalResponses = results[0];
			let proposal = results[1];
			let isGood = false;
		
			console.log(results);

			if (proposalResponses && proposalResponses[0].response &&
				proposalResponses[0].response.status === 200){
				isGood = true;
			} else {
				console.log('Update transaction status proposal was rejected');
				reject('Update transaction status proposal was rejected');
			}

			if (isGood){
				let request = {
					proposalResponses: proposalResponses,
					proposal: proposal
				};

				console.log(`${txIdStr}: Committing updated transaction`);
				return this.channel.sendTransaction(request);
			}
		})
		.then((results) => {
			if (results.status === 'SUCCESS'){
				console.log('update success');
				resolve(txIdStr);
			} else {
				console.log(results);
				reject(`${txIdStr}: commit error`);
			}
		})
		.catch((err) => console.log(err));
		
	});
}


/**
 * Get specific transaction
 *
 **/
BulkPaymentService.prototype.getTransaction = function(key){
	let request = {
		chaincodeId: chaincodeId,
		fcn: getTransactionFcn,
		args: [key]
	};

	return this.channel.queryByChaincode(request)
			.then((payload) => {
				console.log(payload);
				return payload
			});
}

BulkPaymentService.prototype.queryByRange = function(start, end){
    let request = {
        chaincodeId: chaincodeId,
        fcn: queryByDateRangeFcn,
        args: [start, end]
    };

    return this.channel.queryByChaincode(request)
            .then((payload) => {
                return payload.toString('utf8');
            });
}


module.exports = BulkPaymentService;
