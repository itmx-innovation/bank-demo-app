const Client = require('fabric-client')
const path = require('path');
const store_path = path.join(__dirname, '../hfc-key-store');
const chaincodeId = 'bulkpayment';

function EventHubService(peerUri, opts){
    this.client = new Client();
    this.initialized = false;
    this.eh = null;
    this.regid = null;
	this.opts = opts;
	this.peerUri = peerUri;
}

EventHubService.prototype.subscribeEvent = function(onEvent, onError){

	if (this.eh != null){
		this.regid = this.eh.registerChaincodeEvent('bulkpayment', '^bulk*', onEvent, onError);
	} else {

		Client.newDefaultKeyValueStore({path: store_path})
		.then((state_store) => {
			this.client.setStateStore(state_store);
			let crypto_suite = Client.newCryptoSuite();
			let crypto_store = Client.newCryptoKeyStore({path: store_path});
			crypto_suite.setCryptoKeyStore(crypto_store);
			this.client.setCryptoSuite(crypto_suite);
			return this.client.createUser(this.opts);
		})
		.then((user) => {
			this.eh = this.client.newEventHub();
			this.eh.setPeerAddr(this.peerUri);
			this.regid = this.eh.registerChaincodeEvent('bulkpayment', '^bulk*', onEvent, onError);
		})
		.then(() => {
			this.eh.connect();
			console.log('Register for chaincode event successfully');
		})
		.catch((err) => console.log(err));
	}
}

EventHubService.prototype.unsubscribeEvent = function(){
    if (this.regid != null){
        this.eh.unregisterChaincodeEvent(this.regid);
    }
}

module.exports = EventHubService;
